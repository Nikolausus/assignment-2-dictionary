asm=nasm
flg=-felf64
py=python3


.PHONY: test clean


program: main.o dict.o lib.o
	ld -o $@ $^

%.o: %.asm
	$(asm) $(flg) -o $@ $<

main.o: main.asm words.inc lib.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

test: 
	$(py) test.py

clean:
	rm -f *.o program

