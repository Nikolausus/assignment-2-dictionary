section .text

global find_word
extern string_equals

find_word:
	test rsi, rsi
	jz .no
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		test rax, rax
		jnz .yes
		mov rsi, [rsi]
		test rsi, rsi
		jnz .loop
	.no:
		xor rax, rax
		ret
	.yes:
		mov rax, rsi
		ret

