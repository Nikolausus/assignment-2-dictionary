%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define stdin 0
%define stdout 1
%define stderr 2

%define SizeBuffer 256

section .bss
buffer: resb SizeBuffer

section .rodata
err_msg_length:	db 'Error: Kluch slischkom dlinniy', 0
err_msg_exists:	db 'Error: Kluch ne suschestvuet', 0

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, SizeBuffer
	call read_word
	test rax, rax
	je .fail_length
	
	push rdx

	mov rdi, rax
	mov rsi, link	
	call find_word
	test rax, rax
	je .fail_exists
	
	pop rdx

	add rax, 9
	mov rdi, rax
	add rdi, rdx
	mov rsi, stdout
	call print_string
	xor rdi, rdi
	jmp .exit
	
	.fail_length:
		mov rdi, err_msg_length
		jmp .fault_exit

	.fail_exists:
		mov rdi, err_msg_exists
	
	.fault_exit:
		call print_error
		mov rdi, 1

	.exit:
		call exit
