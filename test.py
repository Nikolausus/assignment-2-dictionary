import subprocess

stdin = ["kluch1", "kluch2", "kluch3", "shlyapa na vhod", "a" * 256]

stdout = ["znachenie 1", "drugoe znachenie", "trete znacenie", "-1", "-1"]

stderr = ["-1", "-1", "-1", "Error: Kluch ne suschestvuet", "Error: Kluch slischkom dlinniy"]

print("Start testing...")
results = list()
for index, (inp, out, err) in enumerate(zip(stdin, stdout, stderr), start=1):
    print("------------------------------------\n")
    print("Test #" + str(index))
    process = subprocess.Popen(["./program"],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               universal_newlines=True)

    output, error = process.communicate(inp)
    if process.returncode == 0:
        result = output.strip() == out
    else:
        result = error.strip() == err
    results.append("." if result else "F")
    print("Input: " + inp)
    print("Output: " + output)
    print("Error: " + error)
    print("Test passed!" if result else "Test failed!")
    process.terminate()


print_res = "".join(results)
print("------------------------------------\n")
print("Results: " + print_res)
print("All tests passed" if all(i == '.' for i in results) else "Tests failed")

